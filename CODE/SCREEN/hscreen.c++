# include <iostream>
# include <fstream>
# include <sstream>
# include <string>
# include <cmath>
# include <queue>
# include <vector>

# include <stdlib.h>

using namespace std;

bool TEST(int **sign, double ***G, int id1, int id2, int id3, double lambda, int k)
{
  bool tmpsign = false;

  for (int i=0; i<k; ++i)
    {
      if (sign[i][id2] == sign[i][id3])
	{
	  tmpsign = true;
	}
    }

  if (!tmpsign)
    {
      return false;
    }

  if (fabs(G[id1][id2][id3]) > lambda)
    {
      return true;
    }

  return false;
}

void Identify(bool **S, int *sign, int p)
{
  bool ss[p];
  queue<int> myqueue;

  for (int i=0; i<p; ++i)
    {
      ss[i] = true;
    }

  for (int i=0; i<p; ++i)
    {
      if (!ss[i])
	{
	  continue;
	}
      else
	{
	  ss[i] = false;
	  myqueue.push(i);
	}

      while (!myqueue.empty())
	{
	  int tmp = myqueue.front();
	  sign[tmp] = i;

	  for (int j=0; j<p; ++j)
	    {
	      if (ss[j] && S[tmp][j])
		{
		  ss[j] = false;
		  myqueue.push(j);
		}
	    }
	  myqueue.pop();
	}
    }
}

int main(int argc, char *argv[])
{
  double ***G;
  bool ***S;
  int **sign;

  int p;
  int k;
  double lambda1;
  double lambda2;

  string in_path;
  string out_path;
  string reads;

  stringstream convert;

  ifstream in;
  ofstream out;

  p = atoi(*++argv);
  k = atoi(*++argv);
  lambda1 = atof(*++argv);
  lambda2 = atof(*++argv);
  in_path = *++argv;
  convert << "_" <<lambda1<<"_"<<lambda2<<endl;
  convert >> out_path;
  out_path = "HGROUP_" + in_path + out_path;

  in.open(in_path.c_str());
  out.open(out_path.c_str());

  G = new double **[k];
  S = new bool **[k];
  sign = new int *[k];
  for (int i=0; i<k; ++i)
    {
      sign[i] = new int [p];
      G[i] = new double *[p];
      S[i] = new bool *[p];
      for (int j=0; j<p; ++j)
	{
	  G[i][j] = new double [p];
	  S[i][j] = new bool [p];
	}
    }

  for (int i=0; i<k; ++i)
    {
      for (int j=0; j<p; ++j)
	{
	  sign[i][j] = j;
	  for (int s=0; s<p; ++s)
	    {
	      S[i][j][s] = true;
	    }
	}
    }

  for (int i=0; i<k; ++i)
    {
      for (int j=0; j<p; ++j)
	{
	  for (int s=0; s<p; ++s)
	    {
	      in >> reads;
	      G[i][j][s] = atof(reads.c_str());
	      if (fabs(G[i][j][s]) <= lambda1)
		{
		  S[i][j][s] = false;
		}	      
	    }
	}
    }

  for (int j=0; j<p; ++j)
    {
      for (int s=0; s<p; ++s)
	{
	  double sum = 0;
	  for (int i=0; i<k; ++i)
	    {
	      if (fabs(G[i][j][s]) > lambda1)
		{
		  sum += (fabs(G[i][j][s])-lambda1)*(fabs(G[i][j][s])-lambda1);
		}
	    }
	  if (sum <= lambda2*lambda2)
	    {
	      for (int i=0; i<k; ++i)
		{
		  S[i][j][s] = false;
		}
	    }
	}
    }

  while (1)
    {
      bool break_sign = true;

      // Identify blocks
      for (int i=0; i<k; ++i)
	{
	  Identify(S[i],sign[i],p);
	}

      // See if there are some blocks violating the condition
      for (int j=0; j<p; ++j)
	{
	  for (int s=j+1; s<p; ++s)
	    {
	      for (int i=0; i<k; ++i)
		{
		  if ( (sign[i][j] != sign[i][s]) && TEST(sign,G,i,j,s,lambda1,k))
		    {
		      break_sign = false;
		      S[i][j][s] = S[i][s][j] = true;
		    }
		}
	    }
	}

      if (break_sign)
	{
	  break;
	}
    }


  for (int i=0; i<k; ++i)
    {
      for (int j=0; j<p; ++j)
	{
	  if (sign[i][j] != j)
	    {
	      continue;
	    }

	  out<<j<<" ";

	  for (int s=j+1; s<p; ++s)
	    {
	      if (sign[i][s] == j)
		{
		  out<<s<<" ";
		}
	    }
	  out<<endl<<-1<<endl;
	}
      out<<endl<<-2<<endl;
    }

  in.close();
  out.close();

  return 0;
}
