# include <fstream>
# include <string>
# include <algorithm>
# include <cmath>

# include <Rcpp.h>
# include <stdlib.h>

using namespace Rcpp;
using namespace std;

// [[Rcpp::export]]
void Read(NumericMatrix res, string num1, string num2, string path)
{
  string read_str;
  ifstream in;
  int n;
  int p;

  p = atoi(num1.c_str());
  n = atoi(num2.c_str());
  in.open(path.c_str());

  for (int i=0; i<n*p; ++i)
    {
      for (int j=0; j<p; ++j)
	{
	  in>>read_str;
	  res(i,j) = atof(read_str.c_str());
	}
    }

  in.close();
}
