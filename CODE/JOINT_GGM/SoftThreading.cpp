# include <Rcpp.h>
# include <cmath>

using namespace Rcpp;

//[[Rcpp::export]]
void SOFT_THREADING(NumericMatrix Z, double lambda1, double rho)
{
  int size1 = Z.nrow();
  int size2 = Z.ncol();

  for (int i=0; i<size1; ++i)
    {
      for (int j=0; j<size2; ++j)
	{
	  if (fabs(Z(i,j)) < lambda1/rho)
	    {
	      Z(i,j) = 0;
	    }
	  else if (Z(i,j)>0)
	    {
	      Z(i,j) = Z(i,j) - lambda1/rho;
	    }
	  else
	    {
	      Z(i,j) = Z(i,j) + lambda1/rho;
	    }
	}
    }
}
