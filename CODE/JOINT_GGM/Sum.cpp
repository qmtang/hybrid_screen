# include <iostream>
# include <cmath>
# include <Rcpp.h>

using namespace std;
using namespace Rcpp;

//[[Rcpp::export]]
double SUM(NumericMatrix A, NumericMatrix B)
{
  double sum = 0;
  int size1 = A.nrow();
  int size2 = A.ncol();

  for (int i=0; i<size1; ++i)
    {
      for (int j=0; j<size2; ++j)
	{
	  sum += A(i,j)*B(i,j);
	}
    }

  return sum;
}
