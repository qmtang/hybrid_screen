# include <vector>
# include <iostream>

# include <Rcpp.h>
# include <stdlib.h>

using namespace Rcpp;
using namespace std;

// [[Rcpp::export]]
NumericMatrix RESET(NumericMatrix COV, NumericVector SCREEN)
{
  int size1 = COV.nrow();
  int size2 = COV.ncol();
  NumericMatrix S(size1,size2);
  vector <int> tmp;

  int BLOCK_ID = 0;
  int id = 0;

  while (BLOCK_ID < size1/size2)
    {
      if (SCREEN(id) == -2)
	{
	  ++id;
	  ++BLOCK_ID;
	  continue;
	}

      while (SCREEN(id++) != -1)
	{
	  tmp.push_back((int) (SCREEN(id-1)));
	}

      for (vector <int> ::iterator iter = tmp.begin(); iter != tmp.end(); ++iter)
	{
	  for (vector <int> ::iterator iter2 = tmp.begin(); iter2 != tmp.end(); ++iter2)
	    {
	      S(BLOCK_ID*size2 + (*iter), (*iter2)) = COV(BLOCK_ID*size2 + (*iter), (*iter2));
	    }
	}

      tmp.clear();
    }

  return S;
}
