# include <Rcpp.h>

using namespace Rcpp;

//[[Rcpp::export]]
void COPY(NumericMatrix X, NumericMatrix CX, int index)
{
  int p = CX.ncol();

  for (int i=0; i<p; ++i)
    {
      for (int j=0; j<p; ++j)
	{
	  X(i+(index-1)*p,j)=CX(i,j);
	}
    }
}
