# include <cmath>
# include <Rcpp.h>

using namespace std;
using namespace Rcpp;

//[[Rcpp::export]]
double CAL_PENALTY(NumericMatrix Y, double lambda1, double lambda2, double OBJ)
{
  double sum = 0;
  int size1 = Y.nrow();
  int size2 = Y.ncol();
  int n = size1/size2;

  // Cal L1
  for (int i=0; i<size1; ++i)
    {
      for (int j=0; j<size2; ++j)
	{
	  sum += fabs(Y(i,j));
	}
    }
  sum *= lambda1;

  for (int i=0; i<size2; ++i)
    {
      for (int j=0; j<size2; ++j)
	{
	  double tmp = 0;
	  for (int k=0; k<n; ++k)
	    {
	      tmp += Y(k*size2+i,j)*Y(k*size2+i,j);
	    }
	  sum += lambda2*sqrt(tmp);
	}
    }

  return OBJ+sum;
}
