# include <fstream>
# include <string>

# include <Rcpp.h>
# include <stdlib.h>

using namespace Rcpp;
using namespace std;

// [[Rcpp::export]]
void Read_Screen(NumericVector SCREEN, string path, int n)
{
  ifstream in;
  int readnum;
  int index = 0;

  in.open(path.c_str());

  while (in>>readnum)
    {
      SCREEN(index++) = readnum;
    }

  in.close();
}
