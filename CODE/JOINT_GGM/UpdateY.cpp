# include <cmath>
# include <Rcpp.h>

using namespace Rcpp;

//[[Rcpp::export]]
void UPDATE_Y(NumericMatrix Y, NumericMatrix Z, double lambda2, double rho)
{
  int p = Z.ncol();
  int n = Z.nrow()/p;

  for (int i=0; i<p; ++i)
    {
      for (int j=0; j<p; ++j)
	{
	  double sum = 0;
	  for (int s=0; s<n; ++s)
	    {
	      sum += Z(i+s*p,j)*Z(i+s*p,j);
	    }

	  if (sqrt(sum) < lambda2/rho)
	    {
	      sum = 0;
	    }
	  else
	    {
	      sum = (1-lambda2/rho/sqrt(sum));
	    }

	  for (int s=0; s<n; ++s)
	    {
	      Y(i+s*p,j) = Z(i+s*p,j)*sum;
	    }
	}
    }
}
