# include <Rcpp.h>

using namespace Rcpp;

//[[Rcpp::export]]
void CUT(NumericMatrix COV, NumericMatrix S, int index)
{
  int p = COV.ncol();

  for (int i=0; i<p; ++i)
    {
      for (int j=0; j<p ;++j)
	{
	  S(i,j) = COV(i+(index-1)*p,j);
	}
    }
}
