# include <cmath>
# include <Rcpp.h>
# include <vector>

using namespace Rcpp;
using namespace std;

//[[Rcpp::export]]
NumericMatrix SCREENCUT(NumericMatrix A, NumericVector SCREEN, int BLOCK_ID, int ID)
{
  vector <int> tmp;

  while (SCREEN(ID++) != -1)
    {
      tmp.push_back((int) (SCREEN(ID-1)));
    }

  NumericMatrix T(tmp.size(),tmp.size());

  for (int i=0; i<tmp.size(); ++i)
    {
      for (int j=0; j<tmp.size(); ++j)
	{
	  T(i,j) = A(tmp[i]+BLOCK_ID*A.ncol(),tmp[j]);
	}
    }

  tmp.clear();
  return T;
}
