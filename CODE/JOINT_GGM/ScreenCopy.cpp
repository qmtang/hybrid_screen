# include <cmath>
# include <Rcpp.h>
# include <vector>

using namespace Rcpp;
using namespace std;

//[[Rcpp::export]]
int SCREENCOPY(NumericMatrix X, NumericMatrix CX, NumericVector SCREEN, int BLOCK_ID, int ID)
{
  vector <int> tmp;

  while (SCREEN(ID++) != -1)
    {
      tmp.push_back(SCREEN(ID-1));
    }

  for (int i=0; i<tmp.size(); ++i)
    {
      for (int j=0; j<tmp.size(); ++j)
	{
	  X(tmp[i] + BLOCK_ID*X.ncol(), tmp[j]) = CX(i,j);
	}
    }

  tmp.clear();
  return ID;
}
