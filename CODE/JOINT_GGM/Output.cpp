# include <string>
# include <fstream>
# include <sstream>
# include <cmath>

# include <Rcpp.h>
# include <stdlib.h>

using namespace std;
using namespace Rcpp;

//[[Rcpp::export]]
void OUTPUT(NumericMatrix X, string path, double lambda1, double lambda2)
{
  ofstream out;
  string file_path;
  stringstream convert;

  convert << path;
  convert << "_";
  convert << lambda1;
  convert << "_";
  convert << lambda2;
  convert >> file_path;
  out.open(file_path.c_str());

  int size1 = X.nrow();
  int size2 = X.ncol();
  out.precision(16);

  for (int i=0; i<size1; ++i)
    {
      for (int j=0; j<size2; ++j)
	{
	  if (j)
	    {
	      out<<" ";
	    }
	  out<<X(i,j);
	}
      out<<endl;
    }

  out.close();
}
